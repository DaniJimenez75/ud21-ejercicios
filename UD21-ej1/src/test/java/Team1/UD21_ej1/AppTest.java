package Team1.UD21_ej1;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import dto.calcular;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testSuma()
    {
        double resultado = calcular.calcularOperacion(5, 5, "+", "");
        double esperado = 10.0;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testResta()
    {
        double resultado = calcular.calcularOperacion(15.5, 5, "-", "");
        double esperado = 10.5;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testMultiplicar()
    {
        double resultado = calcular.calcularOperacion(5, 5, "X", "");
        double esperado = 25.0;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testDividir()
    {
        double resultado = calcular.calcularOperacion(20, 5, "/", "");
        double esperado = 4.0;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testRaizCuadrada()
    {
        double resultado = calcular.calcularOperacion(9, 0, "2√X", "");
        double esperado = 3.0;
        assertEquals(esperado, resultado);
    }
    @Test
    public void elevarADos()
    {
        double resultado = calcular.calcularOperacion(5, 0, "x^2", "");
        double esperado = 25.0;
        assertEquals(esperado, resultado);
    }
    @Test
    public void porcentaje()
    {
        double resultado = calcular.calcularOperacion(9, 3, "+", "%");
        double esperado = 9.27;
        assertEquals(esperado, resultado);
    }
    @Test
    public void unoDividido()
    {
        double resultado = calcular.calcularOperacion(16, 0, "1/x", "");
        double esperado = 0.0625;
        assertEquals(esperado, resultado);
    }
}
