package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.calcular;
import exceptions.Excepciones;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JTextArea;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	JButton ce;
	
	JButton c;

	JButton inverso;
	
	JButton elevado;
	
	JButton raizcuadrada2;
	
	JButton dividir;
	
	JButton multiplicacion;
	
	JButton sumar;
	
	JButton igual;
	
	JButton nueve;
	
	JButton seis;
	
	JButton coma;
	
	JButton ocho;
	
	JButton cinco;
	
	JButton cero;
	
	JButton siete;
	
	JButton cuatro;
	
	JButton masMenos;
	
	JButton restar;
	
	JButton uno;
	
	JButton dos;
	
	JButton tres;
	
	JTextPane mostrarOperacion;
	
	JTextPane historial;
	
    JButton porcentaje;
 
	JButton ceBtn;
	 
	JButton cBtn;
	
	String primerValor = "";
	
	String operacion = "";
	
	String operacion2 = "";
	
	double numero1 = 0;
	
	double numero2 = 0;
	
	int longitud = 0;
	
	double resultado = 0;
	
	String resultados = "";
	
	boolean despuesDeOperacion = false;
	
	String valor = "";
	

	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 926, 623);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		ce = new JButton("CE");
		ce.setBounds(178, 168, 71, 31);
		ce.addActionListener(pulsarOperacion);
		
		c = new JButton("C");
		c.setBounds(259, 168, 71, 31);
		c.addActionListener(pulsarOperacion);
		
		JButton delete = new JButton("Delete");				
		delete.setBounds(340, 168, 85, 31);
		contentPane.add(delete);
		delete.addActionListener(pulsarCE);

		 inverso = new JButton("1/x");
		inverso.setBounds(97, 210, 71, 31);
		contentPane.add(inverso);
		inverso.addActionListener(pulsarOperacion);
		
		
		 elevado = new JButton("x^2");
		elevado.setBounds(178, 210, 71, 31);
		contentPane.add(elevado);
		elevado.addActionListener(pulsarOperacion);
		
		 raizcuadrada2 = new JButton("2√X");
		raizcuadrada2.setBounds(259, 210, 71, 31);
		contentPane.add(raizcuadrada2);
		raizcuadrada2.addActionListener(pulsarOperacion);
		
		 dividir = new JButton("/");
		dividir.setBounds(340, 210, 71, 31);
		contentPane.add(dividir);
		dividir.addActionListener(pulsarOperacion);
		
		 multiplicacion = new JButton("X");
		multiplicacion.setBounds(340, 252, 71, 31);
		contentPane.add(multiplicacion);
		multiplicacion.addActionListener(pulsarOperacion);
		
		 sumar = new JButton("+");
		sumar.setBounds(340, 340, 71, 31);
		contentPane.add(sumar);
		sumar.addActionListener(pulsarOperacion);
		
		 igual = new JButton("=");
		igual.setBounds(340, 386, 71, 31);
		contentPane.add(igual);
		
		igual.addActionListener(pulsarIgual);
		
		 nueve = new JButton("9");
		nueve.setBounds(259, 252, 71, 31);
		contentPane.add(nueve);
		nueve.addActionListener(pulsarBoton);
		
		 seis = new JButton("6");
		seis.setBounds(259, 294, 71, 31);
		contentPane.add(seis);
		seis.addActionListener(pulsarBoton);
		
		 coma = new JButton(",");
		coma.setBounds(259, 386, 71, 31);
		contentPane.add(coma);
		coma.addActionListener(pulsarComa);
		
		 ocho = new JButton("8");
		ocho.setBounds(178, 252, 71, 31);
		contentPane.add(ocho);
		ocho.addActionListener(pulsarBoton);
		
		 cinco = new JButton("5");
		cinco.setBounds(178, 298, 71, 31);
		contentPane.add(cinco);
		cinco.addActionListener(pulsarBoton);
		
		 cero = new JButton("0");
		cero.setBounds(178, 386, 71, 31);
		contentPane.add(cero);
		cero.addActionListener(pulsarBoton);
		
		 siete = new JButton("7");
		siete.setBounds(97, 252, 71, 31);
		contentPane.add(siete);
		siete.addActionListener(pulsarBoton);
		
		 cuatro = new JButton("4");
		cuatro.setBounds(97, 298, 71, 31);
		contentPane.add(cuatro);
		cuatro.addActionListener(pulsarBoton);
		
		 masMenos = new JButton("+/-");
		masMenos.setBounds(97, 386, 71, 31);
		contentPane.add(masMenos);
		masMenos.addActionListener(pulsarOperacion);
		
		 mostrarOperacion = new JTextPane();
		mostrarOperacion.setBounds(97, 25, 334, 132);
		contentPane.add(mostrarOperacion);
		
		 historial = new JTextPane();
		historial.setBounds(480, 116, 334, 340);
		contentPane.add(historial);
		
		restar = new JButton("-");
		restar.setBounds(340, 294, 71, 31);
		contentPane.add(restar);
		restar.addActionListener(pulsarOperacion);

		uno = new JButton("1");
		uno.setBounds(97, 340, 71, 31);
		contentPane.add(uno);
		uno.addActionListener(pulsarBoton);

		dos = new JButton("2");
		dos.setBounds(178, 340, 71, 31);
		contentPane.add(dos);
		dos.addActionListener(pulsarBoton);

		tres = new JButton("3");
		tres.setBounds(259, 340, 71, 31);
		contentPane.add(tres);
		tres.addActionListener(pulsarBoton);
		
		porcentaje = new JButton("%");
		porcentaje.setBounds(97, 172, 71, 31);
		contentPane.add(porcentaje);
		porcentaje.addActionListener(pulsarBoton);
		
		ceBtn = new JButton("CE");
		ceBtn.setBounds(178, 172, 71, 31);
		contentPane.add(ceBtn);
		ceBtn.addActionListener(pulsarCE);

		
		cBtn = new JButton("C");
		cBtn.setBounds(259, 172, 71, 31);
		contentPane.add(cBtn);
		cBtn.addActionListener(reset);

		
		
	}
	
	
	//EVENTOS	
			ActionListener pulsarBoton = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {		
						JButton aux = (JButton) e.getSource();
						mostrarOperacion.setText(mostrarOperacion.getText()+aux.getText());//Mostramos valores pulsados
			
						
					}
				};
				
				ActionListener reset = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {		
						mostrarOperacion.setText("");//Reseteamos campo
						
					}
				};
				
				ActionListener pulsarCE = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						valor = mostrarOperacion.getText();
						String cadenaNueva = "";
							for (int i = 0; i < valor.length()-1; i++) {
								cadenaNueva = cadenaNueva + valor.charAt(i);//Borramos el ultimo caracter
							
						}
							mostrarOperacion.setText(cadenaNueva);
						
						
						
					}
				};
				
				
				
				ActionListener pulsarOperacion = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {	
						JButton aux = (JButton) e.getSource();
						primerValor = mostrarOperacion.getText();//Agregamos el primer valor
						numero1 = Double.parseDouble(primerValor);//Lo pasamos a Double
						mostrarOperacion.setText(mostrarOperacion.getText()+aux.getText());
						operacion = aux.getText();	//Guardamos el operador					
						longitud = mostrarOperacion.getText().length();//Guardamos longitud  hasta la operacion
						despuesDeOperacion = true;
						
						
						
						
					}
				};
				
				ActionListener pulsarComa= new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {	
						mostrarOperacion.setText(mostrarOperacion.getText()+".");//Convertimos la coma en punto
		
					}
				};
				
				
				ActionListener pulsarIgual = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						calcular c = new calcular();
						if(!c.mirarOperacion(operacion)) {
							String numero = "";
							for (int i = longitud; i < mostrarOperacion.getText().length(); i++) {
								if(mostrarOperacion.getText().charAt(i) == '%') {//Si hay un % se lo incluimos a operacion2
									operacion2 = "%";
								}else {//Si no simplemente guardamos los valores
									numero = numero + mostrarOperacion.getText().charAt(i);
								}
							
							}
							numero2 = Double.parseDouble(numero);//Lo pasamos a double

						}
						
						//Excepcion en el caso de que se dividida por 0
						try {
							if(numero2 == 0) {
								throw new Excepciones(0);
							}else {
								resultado = c.calcularOperacion(numero1, numero2, operacion, operacion2); //Llamamos al metodo de calculo
								mostrarOperacion.setText(mostrarOperacion.getText()+" = "+resultado+"\n"); 
								resultados = resultados + mostrarOperacion.getText();
								historial.setText(resultados);//Lo imprimimos en historial
							}
							
						}catch (Excepciones ex) {//Si la operacion no es valida
							JOptionPane.showMessageDialog(null, ex.getMessage());
						}					

						
						mostrarOperacion.setText("");//Reiniciamos 

						
						
						
						
					}
				};

				
				
				
				
				
			
			
			
			
			
			
			
}


