package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.calcular;
import exceptions.Excepciones;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JTextArea;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	JButton ce;
	
	JButton c;

	JButton inverso;
	
	JButton elevado;
	
	JButton raizcuadrada2;
	
	JButton dividir;
	
	JButton multiplicacion;
	
	JButton sumar;
	
	JButton igual;
	
	JButton nueve;
	
	JButton seis;
	
	JButton coma;
	
	JButton ocho;
	
	JButton cinco;
	
	JButton cero;
	
	JButton siete;
	
	JButton cuatro;
	
	JButton masMenos;
	
	JButton restar;
	
	JButton uno;
	
	JButton dos;
	
	JButton tres;
	
	JTextPane mostrarOperacion;
	
	JTextPane historial;
	
    JButton parentesisIzquierdo;
 
	JButton barrasX;
	 
	JButton cBtn;
	
	String primerValor = "";
	
	String operacion = "";
	
	String operacion2 = "";
	
	double numero1 = 0;
	
	double numero2 = 0;
	
	int longitud = 0;
	
	double resultado = 0;
	
	String resultados = "";
	
	boolean despuesDeOperacion = false;
	
	String valor = "";
	
	JButton parentesisDerecho;
	
	JButton btnN ;
	
	JButton xElevadoY;
	
	JButton diezElevadoAx;
	
	JButton log;
	
	JButton in;
	
	JButton pi;
	
	JButton dosND;
	
	JButton e;
	
	JButton exp;
	
	JButton mod;
	

	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 926, 623);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		ce = new JButton("CE");
		ce.setBounds(178, 168, 71, 31);
		ce.addActionListener(pulsarOperacion);
		
		c = new JButton("C");
		c.setBounds(259, 168, 71, 31);
		c.addActionListener(pulsarOperacion);
		
		JButton delete = new JButton("Delete");				
		delete.setBounds(339, 207, 85, 31);
		contentPane.add(delete);
		delete.addActionListener(pulsarCE);

		 inverso = new JButton("1/x");
		inverso.setBounds(95, 251, 71, 31);
		contentPane.add(inverso);
		inverso.addActionListener(pulsarOperacion);
		
		
		 elevado = new JButton("x^2");
		elevado.setBounds(12, 251, 71, 31);
		contentPane.add(elevado);
		elevado.addActionListener(pulsarOperacion);
		
		 raizcuadrada2 = new JButton("2√X");
		raizcuadrada2.setBounds(12, 289, 71, 31);
		contentPane.add(raizcuadrada2);
		raizcuadrada2.addActionListener(pulsarOperacion);
		
		 dividir = new JButton("/");
		dividir.setBounds(338, 289, 71, 31);
		contentPane.add(dividir);
		dividir.addActionListener(pulsarOperacion);
		
		 multiplicacion = new JButton("X");
		multiplicacion.setBounds(338, 331, 71, 31);
		contentPane.add(multiplicacion);
		multiplicacion.addActionListener(pulsarOperacion);
		
		 sumar = new JButton("+");
		sumar.setBounds(338, 419, 71, 31);
		contentPane.add(sumar);
		sumar.addActionListener(pulsarOperacion);
		
		 igual = new JButton("=");
		igual.setBounds(338, 465, 71, 31);
		contentPane.add(igual);
		
		igual.addActionListener(pulsarIgual);
		
		 nueve = new JButton("9");
		nueve.setBounds(257, 331, 71, 31);
		contentPane.add(nueve);
		nueve.addActionListener(pulsarBoton);
		
		 seis = new JButton("6");
		seis.setBounds(257, 373, 71, 31);
		contentPane.add(seis);
		seis.addActionListener(pulsarBoton);
		
		 coma = new JButton(",");
		coma.setBounds(257, 465, 71, 31);
		contentPane.add(coma);
		coma.addActionListener(pulsarComa);
		
		 ocho = new JButton("8");
		ocho.setBounds(176, 331, 71, 31);
		contentPane.add(ocho);
		ocho.addActionListener(pulsarBoton);
		
		 cinco = new JButton("5");
		cinco.setBounds(176, 377, 71, 31);
		contentPane.add(cinco);
		cinco.addActionListener(pulsarBoton);
		
		 cero = new JButton("0");
		cero.setBounds(176, 465, 71, 31);
		contentPane.add(cero);
		cero.addActionListener(pulsarBoton);
		
		 siete = new JButton("7");
		siete.setBounds(95, 331, 71, 31);
		contentPane.add(siete);
		siete.addActionListener(pulsarBoton);
		
		 cuatro = new JButton("4");
		cuatro.setBounds(95, 377, 71, 31);
		contentPane.add(cuatro);
		cuatro.addActionListener(pulsarBoton);
		
		 masMenos = new JButton("+/-");
		masMenos.setBounds(95, 465, 71, 31);
		contentPane.add(masMenos);
		masMenos.addActionListener(pulsarOperacion);
		
		 mostrarOperacion = new JTextPane();
		mostrarOperacion.setBounds(54, 13, 405, 132);
		contentPane.add(mostrarOperacion);
		
		 historial = new JTextPane();
		historial.setBounds(478, 195, 334, 340);
		contentPane.add(historial);
		
		restar = new JButton("-");
		restar.setBounds(338, 373, 71, 31);
		contentPane.add(restar);
		restar.addActionListener(pulsarOperacion);

		uno = new JButton("1");
		uno.setBounds(95, 419, 71, 31);
		contentPane.add(uno);
		uno.addActionListener(pulsarBoton);

		dos = new JButton("2");
		dos.setBounds(176, 419, 71, 31);
		contentPane.add(dos);
		dos.addActionListener(pulsarBoton);

		tres = new JButton("3");
		tres.setBounds(257, 419, 71, 31);
		contentPane.add(tres);
		tres.addActionListener(pulsarBoton);
		
		parentesisIzquierdo = new JButton("(");
		parentesisIzquierdo.setBounds(95, 289, 71, 31);
		contentPane.add(parentesisIzquierdo);
		parentesisIzquierdo.addActionListener(pulsarBoton);
		
		barrasX = new JButton("|x|");
		barrasX.setBounds(176, 251, 71, 31);
		contentPane.add(barrasX);
		barrasX.addActionListener(pulsarCE);

		
		cBtn = new JButton("C");
		cBtn.setBounds(258, 207, 71, 31);
		contentPane.add(cBtn);
		
		 parentesisDerecho = new JButton(")");
		parentesisDerecho.setBounds(176, 289, 71, 31);
		contentPane.add(parentesisDerecho);
		
		btnN = new JButton("n!");
		btnN.setBounds(255, 289, 71, 31);
		contentPane.add(btnN);
		btnN.addActionListener(pulsarOperacion);

		 xElevadoY = new JButton("x^y");
		xElevadoY.setBounds(12, 331, 71, 31);
		contentPane.add(xElevadoY);
		xElevadoY.addActionListener(pulsarOperacion);

		 diezElevadoAx = new JButton("10^x");
		diezElevadoAx.setBounds(12, 377, 71, 31);
		contentPane.add(diezElevadoAx);
		diezElevadoAx.addActionListener(pulsarOperacion);

		 log = new JButton("log");
		log.setBounds(12, 419, 71, 31);
		contentPane.add(log);
		log.addActionListener(pulsarOperacion);

		 in = new JButton("In");
		in.setBounds(12, 465, 71, 31);
		contentPane.add(in);
		in.addActionListener(pulsarOperacion);

		dosND = new JButton("2nd");
		dosND.setBounds(13, 207, 71, 31);
		contentPane.add(dosND);
		dosND.addActionListener(pulsarOperacion);

		pi = new JButton("π");
		pi.setBounds(96, 207, 71, 31);
		contentPane.add(pi);
		pi.addActionListener(pulsarOperacion);

		e = new JButton("e");
		e.setBounds(177, 207, 71, 31);
		contentPane.add(e);
		e.addActionListener(pulsarOperacion);

		exp = new JButton("exp");
		exp.setBounds(257, 251, 71, 31);
		contentPane.add(exp);
		exp.addActionListener(pulsarOperacion);

		mod = new JButton("mod");
		mod.setBounds(339, 251, 85, 31);
		contentPane.add(mod);
		cBtn.addActionListener(reset);

		
		
	}
	
	
	//EVENTOS	
			ActionListener pulsarBoton = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {		
						JButton aux = (JButton) e.getSource();
						mostrarOperacion.setText(mostrarOperacion.getText()+aux.getText());//Mostramos valores pulsados
			
						
					}
				};
				
				ActionListener reset = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {		
						mostrarOperacion.setText("");//Reseteamos campo
						
					}
				};
				
				ActionListener pulsarCE = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						valor = mostrarOperacion.getText();
						String cadenaNueva = "";
							for (int i = 0; i < valor.length()-1; i++) {
								cadenaNueva = cadenaNueva + valor.charAt(i);//Borramos el ultimo caracter
							
						}
							mostrarOperacion.setText(cadenaNueva);
																	
					}
				};
				
				
				
				ActionListener pulsarOperacion = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {	
						JButton aux = (JButton) e.getSource();
						primerValor = mostrarOperacion.getText();//Agregamos el primer valor
						numero1 = Double.parseDouble(primerValor);//Lo pasamos a Double
						mostrarOperacion.setText(mostrarOperacion.getText()+aux.getText());
						operacion = aux.getText();	//Guardamos el operador					
						longitud = mostrarOperacion.getText().length();//Guardamos longitud  hasta la operacion
						despuesDeOperacion = true;
						
						
						
						
					}
				};
				
				ActionListener pulsarComa= new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {	
						mostrarOperacion.setText(mostrarOperacion.getText()+".");//Convertimos la coma en punto
		
					}
				};
				
				
				ActionListener pulsarIgual = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						calcular c = new calcular();
						String numero = "";
						if(!c.mirarOperacion(operacion)) {
							
							for (int i = longitud; i < mostrarOperacion.getText().length(); i++) {
									numero = numero + mostrarOperacion.getText().charAt(i);
								}
							numero2 = Double.parseDouble(numero);//Lo pasamos a double

							}else {
								numero2=-1;
							}
								
							

						
						
						//Excepcion en el caso de que se dividida por 0
						try {
							if(numero2 == 0) {
								throw new Excepciones(0);
							}else {
								resultado = c.calcularOperacion(numero1, numero2, operacion, operacion2); //Llamamos al metodo de calculo
								mostrarOperacion.setText(mostrarOperacion.getText()+" = "+resultado+"\n"); 
								resultados = resultados + mostrarOperacion.getText();
								historial.setText(resultados);//Lo imprimimos en historial
							}
							
						}catch (Excepciones ex) {//Si la operacion no es valida
							JOptionPane.showMessageDialog(null, ex.getMessage());
						}					

						
						mostrarOperacion.setText("");//Reiniciamos 

					}
						
						
				
					
				};
}


