package dto;

import javax.swing.JOptionPane;

import exceptions.Excepciones;

public class calcular {
	
	
	
	
	
	public static double calcularOperacion(double numero1, double numero2, String operacion, String operacion2) {
		//OPERACIONES
		double total = 0;
		switch (operacion) {
		case "1/x":
			total = 1/numero1;
			break;		
		case "x^2":
			total = Math.pow(numero1, 2);
			break;
		case "2√X":
			total = Math.sqrt(numero1);
			break;
		case "/":
			total = numero1/numero2;				
			break;
		case "X":
			total = numero1*numero2;
			break;
		case "+":
			total = numero1+numero2;
			break;
		case "-":
			total = numero1-numero2;
			break;
		case "n!":
			total = calcularFactorial(numero1);
			break;
		case "x^y":
			total = Math.pow(numero1, numero2);
			break;
		case "10^x":
			total = Math.pow(10, numero1);
			break;
		case "log":
			total = Math.log10(numero1);

			break;
		case "In":
			total = Math.log(numero1);		
			break;
		case "exp":
			total = expNumero(numero1, numero2);	
			break;

		default:
			break;
		}
		
		//Si tiene el porcentaje
		if(operacion2=="%") {
			if(operacion == "+") {
				total = numero1+((numero2*numero1)/100);
			}else if(operacion == "-") {
				total = numero1-((numero2*numero1)/100);

			}else if(operacion == "X") {
				total = numero1*((numero2*numero1)/100);

			}else if(operacion == "/") {
				total = numero1/((numero2*numero1)/100);

			}
		}
		
		return total;
	}
	
	public boolean mirarOperacion(String operacion) {
		boolean unNumero = false;
		//Si es alguna de estas operaciones
		if(operacion.equals("x^2") || operacion.equals("2√X") || operacion.equals("1/x")
				||operacion.equals("exp") || operacion.equals("n!") || operacion.equals("log")) {
			unNumero = true;
			
		}
		
		return unNumero;
	}
	
	//Calculamos numero factorial
		public static double calcularFactorial(double numero) {
			double factorial = 1;
			
			while(numero!=0) {
				factorial = factorial*numero;
				numero--;			
				
			}
			return factorial;
			 
		}
		
		public static double expNumero(double numero1, double numero2) {
			double resultado  = 0;
			double potenciaDiez = Math.pow(10, numero2);
			resultado = Math.pow(numero1, potenciaDiez);
			return resultado;
		}

}


